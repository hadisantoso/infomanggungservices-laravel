<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Defined Variables
    |--------------------------------------------------------------------------
    |
    | This is a set of variables that are made specific to this application
    | that are better placed here rather than in .env file.
    | Use config('your_key') to get the values.
    |
    */

    'aws_prefix' => env('AWS_PREFIX','https://s3-ap-southeast-1.amazonaws.com/info-manggung/'),
    'jwt_leeway_time' => env('LEEWAY_TIME',10)

];
