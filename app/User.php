<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'api_token', 'firebase_user_id', 'firebase_instance_id', 'photo_profile'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'firebase_user_id', 'firebase_instance_id', 'api_token',
    ];

    public function artists()
    {
        return $this->belongsToMany('App\Artist')->withTimestamps();
    }

    public function firebaseIds()
    {
        return $this->hasMany('App\FirebaseId');
    }

    public function locations()
    {
        return $this->belongsToMany('App\Location')->withTimestamps();;
    }

    public function gigs()
    {
        return $this->belongsToMany('App\Gig')->withTimestamps();;
    }
}
