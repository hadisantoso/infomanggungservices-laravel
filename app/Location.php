<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    public function gigs()
    {
        return $this->hasMany('App\Gig');
    }

    public function users()
    {
        return $this->belongsToMany('App\user')->withTimestamps();
    }
}
