<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FirebaseId extends Model
{
    public function user()
    {
        return $this->belongsTo('App\user');
    }
}
