<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use \Firebase\JWT\JWT;
use GuzzleHttp\Client;
use Auth\RegisterController;
use App\Gig;
use App\User;
use App\Artist;
use Carbon\Carbon;

class GigController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getGigs(Request $request)
    {
        //can select with paginate(number perpage,array column[])
        $gigs = Gig::whereDate('event_date', '>=', Carbon::today()->toDateString())->where('is_published','1')->orderBy('event_date', 'ASC')->with('location')->paginate(10);
        $sliders = Gig::whereDate('event_date', '>=', Carbon::today()->toDateString())->where([['is_gigs_slider','1'],['is_published','1']])->orderBy('event_date', 'ASC')->get();
        // Log::info('tets :'.$sliders);
        return response()->json([
            'gigs' => $gigs,
            'sliders' => $sliders,
            'aws_prefix' => config('constants.aws_prefix'),
        ]);
    }

    public function searchGigs(Request $request)
    {
        //can select with paginate(number perpage,array column[])\
        $gigs = null;
        if($request->has('is_search_all')){
          $gigs = Gig::where('name', 'like', '%' . $request->query_param . '%')->whereDate('event_date', '>=', Carbon::today()->toDateString())->where('is_published','1')->orderBy('event_date', 'ASC')->with('location')->paginate(5);

        }else{
          $gigs = Gig::where('name', 'like', '%' . $request->query_param . '%')->whereDate('event_date', '>=', Carbon::today()->toDateString())->where('is_published','1')->orderBy('event_date', 'ASC')->with('location')->paginate(10);

        }
        return response()->json([
            'gigs' => $gigs,
            'aws_prefix' => config('constants.aws_prefix'),
        ]);
    }

    public function getGigsDetails(Request $request)
    {
        //can select with paginate(number perpage,array column[])\
        $gigs_tmp = Gig::find($request->id);
        $gigs_tmp->click_count = $gigs_tmp->click_count+1;
        $gigs_tmp->save();
        $gigs_data = Gig::with('location')->where('id', $request->id)->first();
        $artists = $gigs_tmp->artists()->get();
        $usergigs = User::find($request->user_id)->gigs()->where('id', $request->id)->first();
        $followed = false;
        if (!empty($usergigs)) {
            $followed = true;
        }
        //Log::info("INFO : ".$artists);
        return response()->json([
            'gigs' => $gigs_data,
            'followed' => $followed,
            'artists' => $artists,
            'aws_prefix' => config('constants.aws_prefix'),
        ]);
    }

    //====Notification dari lokasi dan artist yang difollow====//
    // public function getNotifications(Request $request)
    // {
    //     //$gigs = Gig::orderBy('event_date','ASC')->paginate(10);
    //     $artists = User::find($request->user_id)->artists;
    //     $data = array();
    //     foreach ($artists as $artist) {
    //         foreach ($artist->gigs()->whereDate('event_date', '>=', Carbon::today()->toDateString())->where('is_published','1')->get(['id', 'event_date', 'name', 'place']) as $gig) {
    //             $gig->artist_name = $artist->name;
    //             array_push($data, $gig);
    //         }
    //     }
    //
    //     //sorting by date
    //     usort($data, function ($a1, $a2) {
    //         $v1 = strtotime($a1['event_date']);
    //         $v2 = strtotime($a2['event_date']);
    //         return $v1 - $v2;
    //     }
    //     );
    //
    //     $returnData = array();
    //     $start = ($request->page - 1) * 10;
    //     $end = $start + 10;
    //
    //     if ($end > sizeof($data)) {
    //         $end = sizeof($data);
    //     }
    //
    //     for ($start; $start < $end; $start++) {
    //         array_push($returnData, $data[$start]);
    //     }
    //     return response()->json([
    //         'gigs' => $returnData,
    //         'aws_prefix' => config('constants.aws_prefix'),
    //     ]);
    // }

    public function getNotifications(Request $request){
        $gigs = User::find($request->user_id)->gigs()->whereDate('event_date', '>=', Carbon::today()->toDateString())->where('is_published','1')->orderBy('event_date', 'ASC')->with('location')->paginate(10);
        Log::info($gigs);
        return response()->json([
               'gigs' => $gigs,
               'aws_prefix' => config('constants.aws_prefix'),
           ]);
    }

    public function getGigsUserLocations(Request $request)
    {
        $locations = User::find($request->user_id)->locations()->pluck('id');
        $gigs = Gig::whereHas('location', function ($query) use ($locations) {
            $query->whereIn('id', $locations);
        })->with('location')->whereDate('event_date', '>=', Carbon::today()->toDateString())->where('is_published','1')->orderBy('event_date', 'asc')->paginate(10);

        return response()->json([
            'gigs' => $gigs,
            'aws_prefix' => config('constants.aws_prefix'),
        ]);
    }

    public function getGigsUserRecommendation(Request $request)
    {
        $locations = User::find($request->user_id)->locations()->pluck('id');
        $artist = User::find($request->user_id)->artists()->pluck('id');
        $gigs = Gig::whereDate('event_date', '>=', Carbon::today()->toDateString())->where('is_published','1')->orderBy('event_date', 'asc');
        $gigs = $gigs->where(function ($query) use($locations,$artist){
            $query->whereHas('location', function ($query) use ($locations) {
                $query->whereIn('id', $locations);
            })->orWhereHas('artists',function ($query) use ($artist) {
                $query->whereIn('id', $artist);
            });
        })->with('location')->paginate(10);

        return response()->json([
            'gigs' => $gigs,
            'aws_prefix' => config('constants.aws_prefix'),
        ]);
    }

    public function getGigsByLocation(Request $request){
      $gigs = Gig::whereHas('location', function ($query) use ($request) {
          $query->where('id', $request->location_id);
      })->with('location')->whereDate('event_date', '>=', Carbon::today()->toDateString())->where('is_published','1')->orderBy('event_date', 'asc')->paginate(10);
      return response()->json([
          'gigs' => $gigs,
          'aws_prefix' => config('constants.aws_prefix'),
      ]);
    }

    public function getPopularGigs(Request $request){
      $gigs = Gig::whereDate('event_date', '>=', Carbon::today()->toDateString())->orderBy('click_count','desc')->with('location')->paginate(10);
      return response()->json([
          'gigs' => $gigs,
          'aws_prefix' => config('constants.aws_prefix'),
      ]);
    }

    public function followGigs(Request $request)
    {
        $data = User::find($request->user_id)->gigs()->where('id', $request->gigs_id)->first();
        //Log::info("data ".$data);
        if (empty($data)) {
            $gigs = Gig::find($request->gigs_id);
            $user = User::find($request->user_id);
            $user->gigs()->attach($gigs);
        }
        return "SUCCESS";
    }

    public function unfollowGigs(Request $request)
    {
        $data = User::find($request->user_id)->gigs()->where('id', $request->gigs_id)->first();
        //Log::info("data ".$data);
        if (!empty($data)) {
            $gigs = Gig::find($request->gigs_id);
            $user = User::find($request->user_id);
            $user->gigs()->detach($gigs);
        }
        return "SUCCESS";
    }
}
