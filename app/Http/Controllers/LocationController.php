<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Location;
use App\User;

class LocationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getUserLocations(Request $request)
    {
        $locations = User::find($request->user_id)->locations()->orderBy('location_user.created_at', 'ASC')->paginate(10);
        return response()->json([
            'locations' => $locations,
        ]);
    }

    public function addUserLocation(Request $request)
    {
        $user = User::find($request->user_id);
        $location = Location::where('google_place_id', $request->google_place_id)->first();
        $userlocation = $user->locations()->where('google_place_id', '=', $request->google_place_id)->first();
        if (!empty($userlocation)) {
            return response()->json([
                'location' => $location,
            ]);
        }
        if (empty($location)) {
            $location = new Location;
            $location->google_place_id = $request->google_place_id;
            $location->city_name = $request->city_name;
            $location->save();
        }
        $user->locations()->attach($location);
        return response()->json([
            'location' => $location,
        ]);
    }

    public function deleteUserLocation(Request $request)
    {
        $user = User::find($request->user_id);
        $location = Location::find($request->location_id);
        $user->locations()->detach($location);
        return "SUCCESS";
    }

    public function searchLocation(Request $request){
      $locations = Location::where('city_name', 'like', '%' . $request->query_param . '%')->paginate(5);
      return response()->json([
          'locations' => $locations,
      ]);
    }
}
