<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Log;
use App\FirebaseId;



class GeneralController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function updateFirebaseToken(Request $request)
    {
        Log::info("Update Token : " . $request);
        $firebaseid = FirebaseId::where('instance_id', $request->oldFirebaseInstanceIdToken)->first();
        if ($firebaseid != null) {
            $firebaseid->instance_id = $request->firebaseInstanceIdToken;
            $firebaseid->save();
        }
        return "SUCCESS";
    }
}
