<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use \Firebase\JWT\JWT;
use GuzzleHttp\Client;
use Auth\RegisterController;
use App\Artist;
use App\User;
use Carbon\Carbon;

class ArtistController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getRecommendArtists(Request $request)
    {
        $artists = Artist::whereIn('name', $request->artists)->where('is_published','1')->whereDoesntHave('Users', function ($q) use ($request) {
            $q->where('id', $request->user_id);
        })->get();

        //Log::info("data ".$artists);
        return response()->json([
            'artists' => $artists,
            'aws_prefix' => config('constants.aws_prefix'),
        ]);
    }

    public function followArtist(Request $request)
    {
        $data = User::find($request->user_id)->artists()->where('id', $request->artist_id)->first();
        //Log::info("data ".$data);
        if (empty($data)) {
            $artist = Artist::find($request->artist_id);
            $user = User::find($request->user_id);
            $user->artists()->attach($artist);
        }
        return "SUCCESS";
    }

    public function unfollowArtist(Request $request)
    {
        $data = User::find($request->user_id)->artists()->where('id', $request->artist_id)->first();
        //Log::info("data ".$data);
        if (!empty($data)) {
            $artist = Artist::find($request->artist_id);
            $user = User::find($request->user_id);
            $user->artists()->detach($artist);
        }
        return "SUCCESS";
    }

    public function getFollowedArtists(Request $request)
    {
        $artists = User::find($request->user_id)->artists()->paginate(10);
        return response()->json([
            'artists' => $artists,
            'aws_prefix' => config('constants.aws_prefix'),
        ]);

    }

    public function getArtists(Request $request)
    {
        $artists = Artist::where('is_published','1')->paginate(10, ["id", "name", "photo_profile", "photo_profile_thumbnail"]);
        return response()->json([
            'artists' => $artists,
            'aws_prefix' => config('constants.aws_prefix'),
        ]);

    }

    public function searchArtists(Request $request)
    {
        $artists = null;
        if($request->has('is_search_all')){
          $artists = Artist::where('name', 'like', '%' . $request->query_param . '%')->where('is_published','1')->paginate(6);
        }else{
          $artists = Artist::where('name', 'like', '%' . $request->query_param . '%')->where('is_published','1')->paginate(10);
        }
        return response()->json([
            'artists' => $artists,
            'aws_prefix' => config('constants.aws_prefix'),
        ]);
    }

    public function getArtistDetails(Request $request)
    {
        $artist = Artist::find($request->artist_id);
        $gigs = $artist->gigs()->whereDate('event_date', '>=', Carbon::today()->toDateString())->orderBy('event_date', 'ASC')->with('location')->get();
        $userartist = User::find($request->user_id)->artists()->where('id', $request->artist_id)->first();
        $followed = false;
        if (!empty($userartist)) {
            $followed = true;
        }
        return response()->json([
            'artist' => $artist,
            'followed' => $followed,
            'gigs' => $gigs,
            'aws_prefix' => config('constants.aws_prefix'),
        ]);
    }
}
