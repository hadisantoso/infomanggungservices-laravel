<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Location;
use App\Gigs;
use App\Artist;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;


class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getCountData(Request $request)
    {
        $artists = User::find($request->user_id)->artists;
        // $gigs = array();
        // foreach ($artists as $artist) {
        //     foreach ($artist->gigs()->whereDate('event_date', '>=', Carbon::today()->toDateString())->get(['id', 'event_date', 'name', 'place']) as $gig) {
        //         $gig->artist_name = $artist->name;
        //         array_push($gigs, $gig);
        //     }
        // }
        // $gigs = count($gigs);
        $locations = User::find($request->user_id)->locations()->orderBy('location_user.created_at', 'ASC')->count();
        $artists = User::find($request->user_id)->artists()->count();
        $gigs = User::find($request->user_id)->gigs()->whereDate('event_date', '>=', Carbon::today()->toDateString())->where('is_published','1')->count();

        return response()->json([
            'count_gigs' => $gigs,
            'count_locations' => $locations,
            'count_artists' => $artists,
        ]);
    }
}
