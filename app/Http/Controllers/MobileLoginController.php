<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use \Firebase\JWT\JWT;
use GuzzleHttp\Client;
use Auth\RegisterController;
use App\User;
use App\FirebaseId;

class MobileLoginController extends Controller
{
    public function login(Request $request)
    {
        $client = new Client();
        $publicKeyURL = 'https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com';
        $res = $client->get($publicKeyURL);
        $key = json_decode($res->getBody(), true);
        $cacheControl = $res->getHeaderLine('Cache-Control');
        //Log::info("test : ".$request);
        JWT::$leeway = config('constants.jwt_leeway_time');
        $decoded = JWT::decode($request->firebaseIdToken, $key, ["RS256"]);
        $decoded_array = (array)$decoded;
        //Log::info(print_r($decoded_array,true));
        $randomPassword = str_random(10);
        $user = User::where('email', $decoded_array['email'])->first();

        if ($user != null) {
            $firebaseid = FirebaseId::where('instance_id', $request->firebaseInstanceIdToken)->first();
            if ($firebaseid == null) {
                $firebaseid = new FirebaseId();
                $firebaseid->instance_id = $request->firebaseInstanceIdToken;
                $user->firebaseIds()->save($firebaseid);
            } else {
                $user->firebaseIds()->save($firebaseid);
            }
            return response()->json([
                'user' => $user->makeVisible('api_token'),
            ]);
        }

        $name = '';
        if($request->has('name')) {
            if (array_key_exists('name', $decoded_array)) {
                $name = $decoded_array['name'];
            } else {
                $name = 'Anonymous';
            }
        }

        $photo_profile = '';
        if (array_key_exists('picture', $decoded_array)) {
            $photo_profile = $decoded_array['picture'];
        }
        $user = User::create([
            'name' => $name,
            'email' => $decoded_array['email'],
            'photo_profile' => $photo_profile,
            'api_token' => str_random(60),
            'password' => app('hash')->make($randomPassword),
            'firebase_user_id' => $decoded_array['user_id'],
        ]);

        $firebaseid = FirebaseId::where('instance_id', $request->firebaseInstanceIdToken)->first();
        if ($firebaseid == null) {
            $firebaseid = new FirebaseId();
            $firebaseid->instance_id = $request->firebaseInstanceIdToken;
            $user->firebaseIds()->save($firebaseid);
        } else {
            $user->firebaseIds()->save($firebaseid);
        }

        return response()->json([
            'user' => $user->makeVisible('api_token'),
            'newuser' => '1',
        ]);
    }
}
