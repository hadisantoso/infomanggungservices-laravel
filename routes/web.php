<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/v1/login', 'MobileLoginController@login');


$router->group(['prefix' => 'v1'], function () use ($router) {
    $router->post('/updatefirbasetoken', 'GeneralController@updateFirebaseToken');
    $router->post('/artists', 'ArtistController@getArtists');
    $router->post('/searchartists', 'ArtistController@searchArtists');
    $router->post('/getartistdetails', 'ArtistController@getArtistDetails');
    $router->post('/recommendartists', 'ArtistController@getRecommendArtists');
    $router->post('/followartist', 'ArtistController@followArtist');
    $router->post('/unfollowartist', 'ArtistController@unfollowArtist');
    $router->post('/followedartists', 'ArtistController@getFollowedArtists');
    $router->post('/gigs', 'GigController@getGigs');
    $router->post('/searchgigs', 'GigController@searchGigs');
    $router->post('/getgigsdetails', 'GigController@getGigsDetails');
    $router->post('/gigsbyuserlocations', 'GigController@getGigsUserLocations');
    $router->post('/gigsbyuserrecommendation', 'GigController@getGigsUserRecommendation');
    $router->post('/gigsbylocation', 'GigController@getGigsByLocation');
    $router->post('/getpopulargigs', 'GigController@getPopularGigs');
    $router->post('/notifications', 'GigController@getNotifications');
    $router->post('/followgigs', 'GigController@followGigs');
    $router->post('/unfollowgigs', 'GigController@unfollowGigs');
    $router->post('/locations', 'LocationController@getUserLocations');
    $router->post('/addlocation', 'LocationController@addUserLocation');
    $router->post('/deletelocation', 'LocationController@deleteUserLocation');
    $router->post('/searchlocation', 'LocationController@searchlocation');
    $router->post('/getcountdata', 'ProfileController@getCountData');
});
